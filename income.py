class Income:
    def __init__(self):
        self.incomes = {}

    def add_user(self, name, income):
        name_of_user = name
        income_of_user = income
        self.incomes[name_of_user] = income_of_user

    def calculate_tax(self, name):
        base_income = self.incomes[name]
        if base_income <= 160000:
            tax = 0

        elif (base_income > 160000) and (base_income <= 500000):
            tax = (base_income-160000)*0.1

        elif (base_income > 500000) and (base_income <= 800000):
            tax = (base_income-500000)*0.2 + 34000

        else:
            tax = (base_income-800000)*0.3 + 94000

        return tax